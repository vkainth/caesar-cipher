require 'sinatra'
if development?
  require 'sinatra/reloader'
end

SHIFT = 1
ALPHABETS = 'abcdefghijklmnopqrstuvwxyz'.split('')

get '/' do
  erb :index
end

get '/encode' do
  encode = params['text']
  if encode
    encoded = encode_text(encode.downcase)
  else
    encoded = nil
  end
  erb :encode, locals: { encoded: encoded }
end

get '/decode' do
  decode = params['text']
  if decode
    decoded = decode_text(decode.downcase)
  else
    decoded = nil
  end
  erb :decode, locals: { decoded: decoded }
end

def encode_text(text)
  encoded = []
  text.split('').each do |char|
    if ALPHABETS.include? char
      idx = find_index(char)
      encoded.push(ALPHABETS[idx])
    else
      encoded.push(char)
    end
  end
  encoded.join('')
end

def find_index(character, shift = 1, encode = true)
  if encode
    idx = ALPHABETS.index(character) + shift
    if idx == ALPHABETS.length
      idx = 0
    end
  else
    idx = ALPHABETS.index(character) - shift
    if idx < 0
      idx = ALPHABETS.length - 1
    end
  end
  idx
end

def decode_text(text)
  decoded = []
  text.split('').each do |char|
    if ALPHABETS.include? char
      idx = find_index(char, 1, false)
      decoded.push(ALPHABETS[idx])
    else
      decoded.push(char)
    end
  end
  decoded.join('')
end
