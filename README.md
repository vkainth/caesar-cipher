# Caesar's Cipher

[https://powerful-brook-14923.herokuapp.com](https://powerful-brook-14923.herokuapp.com/)

## Introduction

Caesar's Cipher is an age-old encryption method used to hide the contents of
your text. [Read More](https://en.wikipedia.org/wiki/Caesar_cipher)

## Installation

- Download or clone the repository

- In the directory of the project, run `bundle install`

- Run `ruby index.rb` to start the local server

- Use heroku [deployment](https://devcenter.heroku.com/articles/git) for production

## Enhancements

- Style the app

- Add a custom shifting value
