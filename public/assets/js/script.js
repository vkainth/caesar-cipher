document.querySelector('.navbar-burger').addEventListener('click', function() {
    let toggle = document.querySelector('.navbar-burger');
    let menu = document.querySelector('.navbar-menu');
    toggle.classList.toggle('is-active');
    menu.classList.toggle('is-active');
});
